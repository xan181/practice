const fruits =[
  {id:1, title: 'Яблоки', price:20, img:"https://e1.edimdoma.ru/data/ingredients/0000/2374/2374-ed4_wide.jpg?1487746348"},
  {id:2, title: 'Апельсин', price:30, img:"https://skin.ru/storage/ingredients/943/conversions/xYWmKL5Kcg8x4zEynPFoVwjIal5wuolHBDCbKiUk-preview_3_2.jpg"},
  {id:3, title: 'Манго', price:40, img:"https://img.artlebedev.ru/everything/illustrations/kolontai/images/Mango.jpg"},
]

const modal = $.modal({
  title: 'Jeka Modal',
  closable: true,
  content: `
  <h4>Modal is working</h4>
  <p>Lorem ipsum dolor sit.</p>
  `,
  width: '400px',
  footerButtons: [
    {text:"Ok", type:"primary", handler(){
        console.log("Primary btn clicked")
        modal.close()
    }},
    {text:"Cancel", type:"danger", handler(){
        console.log("Danger btn clicked")
        modal.close()
      }}
  ]
})